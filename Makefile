PKGNAME := snowslurm

buikd:
	R CMD build snowslurm

check:
	R CMD check snowslurm

install:
	R CMD INSTALL snowslurm_*.*.tar.gz

clean:
	rm -rf ${PKGNAME}_*.tar.gz  ${PKGNAME}.Rcheck
